function [kappa] = get_Curvature(P, epsilon)
% -------------------------------------------------------------------
% calculates the mean curvature flow of a matrix (Kappa)
% P       | the n-th phi(x,y) matrix
% epsilon | a user specificed constant (epsilon << 1)
%         | also defines the order of numerical precision
% -------------------------------------------------------------------


[m,n] = size(P);
% pad the boundary of P with duplicate ghost points
% -------------------------------------------------------------------
P = padarray(P,[1,1],'replicate','both');


% define terms for the central difference scheme
% -------------------------------------------------------------------
% p = phi(i  , j)
% -------------------------------------------------------------------
% a = phi(i+1, j)  |  c = phi(i, j+1)
% b = phi(i-1, j)  |  d = phi(i, j-1)
% -------------------------------------------------------------------
% aa = phi(i+1, j+1)  |  cc = phi(i+1, j-1)
% bb = phi(i-1, j+1)  |  dd = phi(i-1, j-1)
% -------------------------------------------------------------------
a = P(3:m+2, 2:n+1);
b = P(1:m  , 2:n+1);
c = P(2:m+1, 3:n+2);
d = P(2:m+1, 1:n  );
p = P(2:m+1, 2:n+1);
aa = P(3:m+2, 3:n+2);
bb = P(1:m  , 3:n+2);
cc = P(3:m+2, 1:n  );
dd = P(1:m  , 1:n  );


% approximate the partial derivatives of phi
% use central difference schemes
% assume h = delta_x = delta_y = 1
% -------------------------------------------------------------------
Dx = (a-b) * 0.5;
Dy = (c-d) * 0.5;
Dxx = a - 2*p + b;
Dyy = c - 2*p + d;
Dxy = (aa - cc - bb + dd) * 0.25;


% calculate the mean curvature flow
% epsilon avoids division by zero & specifies the numerical precision
% -------------------------------------------------------------------
g = sqrt(Dx.^2 + Dy.^2);
k = Dyy.*(Dx.^2) + Dxx.*(Dy.^2) -  2.0*(Dxy.*Dx.*Dy)...
    ./ (g.^3 + epsilon);


% clamp the approximation of kappa between [-k_max,k_max]
% where k_max is based on the discretized grid
% avoids kappa -> infinity
% -------------------------------------------------------------------
% kappa_max = 1/radius = 1 / max(delta_x, delta_y)
% assume h = delta_x = delta_y = 1
% -------------------------------------------------------------------
k_max = ones([m n]);
kappa = max(-1.*k_max, min(k,k_max));