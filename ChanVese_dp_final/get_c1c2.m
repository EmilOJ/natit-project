% Returns the constants c1 and c2 for use in the
% original Chan-Vese image segementation algorithm.
% 
% Note: The method below calculates these constants
% as discretized sums.
% ---------------------------------------------------------
function [c1,c2] = get_c1c2(I,P,epsilon)
	H = Heavy(P,epsilon);
	c1 = sum(sum(I .* H)) ./ sum(sum(H));
	c2 = sum(sum(I .* (1-H))) ./ sum(sum(1-H));
end