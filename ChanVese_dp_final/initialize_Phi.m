function [P] = initialize_Phi(I,phi)
% creates a simple signed distance map

	% get number of rows & columns
  [m,n] = size(I);
  
  % gets the matix's indices  
  row_index = repmat([1:m],n,1);
  col_index = row_index.';

  % generate an initial signed distance map, phi_0
  if (phi == 0) 
    % method = 'checkerboard'
    P = sin(row_index(:) .* (pi/5)) .* sin(col_index(:) .* (pi/5));
    P = reshape(P,[m n]);
  end

  if (phi == 1)
  	% method = 'elipse'
	  P = sqrt( (row_index(:) - m/2).^2 + (col_index(:) - n/2).^2 ) -...
          min(m,n)/2.5;
      P = reshape(P,[m n]);
  end