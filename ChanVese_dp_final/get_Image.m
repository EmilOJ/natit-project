function [I,file_name] = get_Image(full_path, add_noise)
% pre-processing of an image file for Chan-Vese segmentation

% -------------------------------------------------------------------
% full_path    | full image file path
% add_noise    | 0 = FALSE, 1 = TRUE
%
%
  % set any non-existant variables
  % 0 = FALSE
  % 1 = TRUE
  % -----------------------------------------------------------------
  if (~exist('full_path','var'))
  	% prompt user for image file
    [file_name, dir_path] = ...
    uigetfile({'*.jpg;*.tif;*.png;*.gif;*.bmp','Image File';...
               '*.*','All Files'},...
               'Select an image file to segment');
    full_path = [dir_path, file_name];
  end

  if (~exist('add_noise','var'))
    % set default to FALSE
    add_noise = 0;
  end

  I = imread(full_path);  % class == 'uint8'

  [m,n,d] = size(I);

  if (d>2)
    I = rgb2gray(I);        % flatten any 3D image information
  end

  if (add_noise == 1)
    I = imnoise(I,'gaussian');
  end

  I = double(I);

end