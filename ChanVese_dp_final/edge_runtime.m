function [] = edge_runtime(file)
	I = rgb2gray(imread(file));
	[~, treshold] = edge(I, 'sobel');
	fudgeFactor = .5;
	BWs = edge(I, 'sobel', treshold * fudgeFactor);
	figure, imshow(BWs), title('Segmentation');
end