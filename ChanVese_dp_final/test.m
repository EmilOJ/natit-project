function [result] = test(x)
% -------------------------------------------------------------------
% 1 = TRUE
% 0 = FALSE
% -------------------------------------------------------------------

I = ceil(255 .* rand(x));
size(I)
I = uint8(I);
P = initialize_Phi(I,0);

figure(2);
title('Live Segmentation');
imagesc(P);
colormap(gray);
hold on;
[c,h] = contour(P, [0 0], 'r');
waitforbuttonpress
delete(h(1));
waitforbuttonpress

for i=1:9
	[c,h] = contour(P, [i*0.1, i*0.1], 'r');
	waitforbuttonpress
	delete(h(1));
end

hold off;