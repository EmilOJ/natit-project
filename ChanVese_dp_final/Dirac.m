% This implements an approximation and regularization
% of the Dirac delta function, delta(z), for use in the
% original Chan-Vese image segementation algorithm
%
% NOTE: In their original experiments, the value of
% epsilon is set to 1.
% ---------------------------------------------------------
function [z] = Dirac(z,epsilon)
	if epsilon == 1
	  	z = 1 ./ (  pi *(z.^2 + 1) );
	else
	  	z = 1 ./ ( (pi * epsilon)*(1 + (z.^2 ./ epsilon^2)) );
end