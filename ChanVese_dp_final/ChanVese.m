% Chan-Vese image segementation algorithm
% ---------------------------------------------------------
% T. Chan and L. Vese, Active contours without edges.
% IEEE Transactions on Image Processing, vol. 10, no. 2,
% pp. 266-277, Feb. 2001.
%
%   -Initialize phi_0
%   -Compute c_1 and c_2
%   -Solve the PDE for phi_(n+1)
%   -Reinitialze phi locally (optional)
%   -If solution is stationary terminate
%    (Else iterate)
%
% ---------------------------------------------------------
function [P_0] = ChanVese(file, phi)


	% pre-process image file
	if (~exist('file', 'var'))
		[I,file_name] = get_Image();
	else
		[I,file_name] = get_Image(file,0);
	end


	% create a signed distance map
	if (~exist('phi', 'var'))
		% default method = checkerboard
		phi = 1;
    end
   
    P_0 = initialize_Phi(I, phi);


	% set default constant values
	delta_x = 1;
	delta_t = 0.01;
	mu = 255;
	nu = 0;
	lambda_1 = 1;
	lambda_2 = 1;
	eps_H = 1;        % epsilon for heavyside
	eps_D = 1;        % epsilon for dirac
	eps_K = 0.00000001;    % epsilon for kappa
  max_iterations = 20000;
  [c1,c2] = get_c1c2(I, P_0, eps_H);
    
  % show initialization
  figure(1);
	imagesc(I);
	colormap(gray);
	title(sprintf(['Initialized Contour'...
                 'Iteration = 0     '...
                 'V_{max} = ?     '...
                 'delta_t = ?\n'...
                 'c1 = ?     '...
                 'Dc1 = ?\n'...
                 'c2 = ?     '...
                 'Dc1 = ?'],...
                 c1,c2));
	%colorbar;
	%axis image;
	hold on;
	[C,h] = contour(P_0, [0 0], 'r', 'LineWidth', 2);
	hold off;
  %saveas(1, [file_name(1:find(file_name=='.')-1) '_0.jpg']);

    
	% solve the PDE
	for i=1:max_iterations
	  [c1,c2] = get_c1c2(I, P_0, eps_H);
	  kappa = get_Curvature(P_0, eps_K);
	  V = Dirac(P_0, eps_D) .* (...
        (mu .* kappa) - (nu) -...
        (lambda_1 .* (I - c1).^2) +...
        (lambda_2 .* (I - c2).^2)...
        );
	  Vmax = max(abs(V(:)));
	  delta_t = max(delta_t, delta_x / Vmax);
	  P_1 = P_0 + (delta_t * V);
      
    ci1 = c1;
    ci2 = c2;
    [cn1,cn2] = get_c1c2(I, P_1, eps_H);
    Dc1 = (abs(ci1 - cn1) * 100) / ci1; 
    Dc2 = (abs(ci2 - cn2) * 100) / ci2;
      
	  % plot real-time
	  figure(1);
	  imagesc(I);
	  colormap(gray);
	  title(sprintf(['Live Segmentation \n'...
                   'Iteration = %d     '...
                   'V_{max} = %0.3f     '...
                   'delta_t = %0.3f\n'...
                   'c1 = %0.3f     '...
                   'Dc1 = %0.6f \n'...
                   'c2 = %0.3f     '...
                   'Dc1 = %0.6f '...
                   ],i,Vmax,delta_t,c1,Dc1,c2,Dc2));
	  %colorbar;
	  %axis image;
	  hold on;
	  [C,h] = contour(P_1, [0 0], 'r', 'LineWidth', 2);
	  hold off;

      % save image intermediately
      if (i==100)
        colorbar;
        axis image;
        saveas(1,'output_100.jpg') 
      end
      
      % check stop criteria
      eps_c = 0.001;
      eps_V = 0.5;
      if ((Dc1<eps_c) & (Dc2<eps_c)) | (Vmax < eps_V)
        colorbar;
        axis image;
        saveas(1,'output_final.jpg')
        break
      end
      
      % update P_0
      P_0 = P_1;
  end

colorbar;
axis image;
saveas(1,'output2.jpg') 

end

