% Chan-Vese image segementation algorithm
% ---------------------------------------------------------
% T. Chan and L. Vese, “Active contours without edges,�?
% IEEE Transactions on Image Processing, vol. 10, no. 2,
% pp. 266–277, Feb. 2001.
%
%   -Initialize phi_0
%   -Compute c_1 and c_2
%   -Solve the PDE for phi_(n+1)
%   -Reinitialze phi locally (optional)
%   -If solution is stationary terminate
%    (Else iterate)
%
% ---------------------------------------------------------
function [] = ChanVese()

    close all;
	clear all;
	clc;

    % locate and read image data
	noise = false;
	%I = get_Image(noise);
	
	%Using the same image over and over and over...
	I = double(rgb2gray(imread('Beck_Sea-Change.jpg')));

	%adding frame/boundary copying values from I
	[imax,jmax] = size(I);
	J = ones(imax+2,jmax+2);
	J(1,2:jmax+1) = I(1,:);
	J(end,2:jmax+1) = I(end,:);
	J(2:imax+1,1) = I(:,1);
	J(2:imax+1,end) = I(:,end);
	
	J(2:imax+1,2:jmax+1) = I;
	I = J;

	% create a simple signed distance field, based on image size
    phi = zeros(imax+2,jmax+2);
    for i=1:imax+2
	    for j=1:jmax+2
	    	% OPTION 1:
    	    % start with an elipse/circle
    	    phi(i,j) = sqrt( (i-imax/2)^2 + (j-jmax/2)^2 ) - min(imax,jmax)/3;

    	    % OPTION 2:
    	    % start with a sinus grid (faster convergence, less precision)
    	    % phi(i,j) = sin(i*pi/5)*sin(j*pi/5);
	    end
    end

	% initialize PDE parameters
	delta_t = 0.1;
	delta_t_eps = 0.000001;
	lam1 = 1;
	lam2 = 1;
	mu = 1;
	nu = 0;
    E_dirac = 1;
    h = 1;
    itcounter = 1;
    phi0 = phi;
    vmax = 0.1;
    kmax = 1/h;
    time1 = clock();
    t = true;
    difference_eps = 0.001;

	% solve the PDE
	while t == true;
    	%Getting averages, including boundary
    	[c1,c2] =get_c1c2(I,phi,E_dirac);

    	%Getting averages, excluding boundary of I, but also of phi
    	%[c1,c2] =get_c1c2(I(2:imax+1,2:jmax+1),phi(2:imax+1,2:jmax+1),E_dirac);

		for i=2:imax+1  

			for j=2:jmax+1 
				
				%Curvature - divergent term
				kij = (((phi(i+1,j) - 2*phi(i,j) +phi(i-1,j))) /...
				(sqrt((phi(i+1,j) - phi(i,j))^2/(h^2) +...
				(phi(i,j+1) - phi(i,j-1))^2/(2*h)^2)+eps) +...
				((phi(i,j+1) - 2*phi(i,j) +phi(i,j-1))) /...
				(sqrt((phi(i,j+1) - phi(i,j))^2/(h^2) +...
				(phi(i+1,j) - phi(i-1,j))^2/(2*h)^2))+eps);

				% Clamping curvature
				kij = max(-kmax,min(kij,kmax));

				% _________
				vij = Dirac(phi(i,j),E_dirac)* (...
				(mu/h^2)*kij - nu - lam1*((I(i,j)-c1)^2) +...
				lam2*((I(i,j)-c2)^2));

				%Variable timestep
				if abs(vij) > abs(vmax)
					vmax = abs(vij);
				end

				% computing phi(n+1,i,j)
				phi0(i,j) = phi(i,j) + delta_t*vij;
			end;
			
		end;
		
		%updating timestep
		delta_t = h/(vmax+eps)
		
		itcounter = itcounter +1
		
		%stopping criterias
		difference = phi - phi0;
		if itcounter > 10000;
			disp('Max iterations reached');
			break
		elseif delta_t < delta_t_eps;
			disp('Time step is to small')
			break
		elseif max(difference(:)) < difference_eps;
			disp('The change in phi is small')
			break
		end
		
		%updating phi
		phi = phi0;

		% plot the evolving curve in real time
		figure(2);
		imagesc(I);
		colormap(gray);
		hold on;
		title('Live Segmentation');
		contour(phi, eps, 'r');
		hold off;
	
	end
	%timing
	time = clock() - time1;
	
	%plotting contour
	figure(3);
	hold on;
	contour(phi,eps,'r')
	axis ij;
	hold off;

end