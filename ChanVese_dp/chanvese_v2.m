close all;
clear all;
clc;

%reading image
prompt = 'what is the filename of the image'
filename = input(prompt,'s')

%convert image to bw - size is image
%U = double(im2bw( imread(filename) ));

%convert image to bw - size fixed 200 x 200
U = imread(filename);
U = double(im2bw(imresize(U,[200,200])));

%show figure
%figure()
%imagesc(U)
%colormap(gray)

% Create a simple signed distance field, based on image size
[imax,jmax] = size(U);
phi = zeros(imax,jmax);
for i=1:imax
	for j=1:jmax
    	phi(i,j) = sqrt( (i-imax/2)^2 + (j-jmax/2)^2 ) - min(imax,jmax)/2 ;
	end
end


% Plot the signed distance field
%figure();
%imagesc(phi);
%colormap(gray);
%hold on;
%contour(phi,0.01, 'r');
%hold off;

% Chan vese implementation
prompt = 'How many iterations';
maxit = input(prompt);

%initialization
delta_t = 0.1;
c1 = 0;
c2 = 1;
lam1 = 0.5;
lam2 = 0.5;

%dirac = epsilon/(pi*(phi(i,j)^2 + epsilon^2);
epsilon = 0.001;


%plotting intial curve and image
figure(1)
imagesc(U)
colorbar;
axis image;
hold on;
title('Initial Image & Contour')
contour(phi,0.01, 'r')
colormap(gray)
hold off;

%Saving intial curve and image
saveas(1,'output1.jpg')

time1 = clock();

for k=1:maxit % no of iterations
	for i=1:imax
    	for j=1:jmax
        	phi(i,j) = phi(i,j) + delta_t  ...
        	* (-lam1*((U(i,j)-c1)^2) + lam2* (U(i,j)-c2)^2); % ...
        	% * epsilon/(pi*(phi(i,j)^2 + epsilon^2));
    	end
	end

%plots of evolving curve
%figure(2)
%imagesc(U)
%axis image;
%hold on;
%title('Live Segmentation')
%contour(phi,0.01, 'r')
%colormap(gray)
%hold off;
end

time2 = clock();
runtime = etime(time2,time1)

%Saving final segmentation
figure(2)
imagesc(U)
colorbar;
axis image;
hold on;
title('Final segmentation')
contour(phi,0.01, 'r')
colormap(gray)
hold off;
saveas(2,'output2.jpg')

figure(3)
title('Final Contour')
hold on;
contour(phi,0.01, 'r')
axis ij;
hold off;
saveas(3,'output3.jpg')


