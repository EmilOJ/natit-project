% Returns the constants c1 and c2 for use in the
% original Chan-Vese image segementation algorithm.
% 
% Note: The method below calculates these constants
% as discretized sums.
% ---------------------------------------------------------
function [c1,c2] = get_c1c2(u0,phi_n,E)
	H = Heavy(phi_n,E);
	c1 = sum(sum(u0 .* H)) / sum(sum(H));
	c2 = sum(sum(u0 .* (1-H))) / sum(sum(1-H));
end