% This implements an approximation and regularization
% of the Heavyside function, H(z), for use in the
% original Chan-Vese image segementation algorithm
%
% Note: In their original experiments, the value of
% epsilon is set to 1.
% ---------------------------------------------------------
function [x] = Heavy(z,E)
	if E == 1
	    x = 0.5 + (atan(z) / pi);
	else
	  	x = 0.5 + (atan(z/E) / pi);
end