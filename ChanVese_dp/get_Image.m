% ---------------------------------------------------------
function [I] = get_Image(noise)

  	% get location of image file
    [file_name, path_name] = uigetfile({'*.jpg;*.tif;*.png;*.gif;*.bmp','Image File';...
                                        '*.*','All Files'},...
                                        'Select an image file to segment');
    full_path = [path_name, file_name];

    % read image data
    I = imread(full_path);

    %adds noise
    if noise;
      I = imnoise(I,'gaussian');
    end

    %convert image to double, grayscale 0:255
    I = double(rgb2gray(I));

end